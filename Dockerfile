FROM node
WORKDIR /src
COPY . .
RUN npm install express mysql2
CMD node server.js
EXPOSE 3000